#!/usr/bin/env bash

MY_PATH="`dirname \"$0\"`"
CATKIN_FOLDER='/home/initega/catkin_ws'
fifo="/tmp/ros-handler$(date +%s%N)"
started_file="$MY_PATH/ros-started-transmitting$(date +%s%N)"
identifier="0"
pause_file="$MY_PATH/pause$(date +%s%N)"
rospid_file="$MY_PATH/roslaunch.pid"
listener="$MY_PATH/demo-listener.py"
change_options_key='z'
init_path="$MY_PATH/default-init.csv"

while getopts ":i:" option; do
    case "${option}" in
        i)
            init_path="${OPTARG}"
            ;;
        *)
			echo "Hasieraketa datubasea behar da. -i erabili horretarako." >&2
            ;;
    esac
done
shift $((OPTIND-1))


# Function to call to stop listener in case of SIGINT
stop_listener() {
	kill -s INT "$listener"
	end='1'
}

paused='1'
if [ $paused -eq 1 ]; then
	touch "$pause_file"
else
	rm "$pause_file" 2>/dev/null
fi

# Running the tools needed
echo "Sourcing setup.bash..."
. "$CATKIN_FOLDER/devel/setup.bash"
if [ ! -f "$MY_PATH/roslaunch.pid" ]; then
	echo "Launching ROS y3space_driver..."
	echo "====================================================
Keep the IMU looking North, still and DON'T TOUCH IT
===================================================="
	sleep 1
	echo "Starting ROS in 3"
	sleep 1
	echo "Starting ROS in 2"
	sleep 1
	echo "Starting ROS in 1"
	sleep 1
	{
		master="$$"
		mkfifo "$fifo"
		roslaunch y3space_driver driver.launch > "$fifo" &
		roslaunch="$!"
		echo "Saving ROS pid on roslaunch.pid"
		echo "$roslaunch" > "$rospid_file"
		while read -r line; do
			if [ "$line" = "START" ]; then
				touch "$started_file"
			fi
		done < "$fifo"
		rm "$fifo"
	} &
	wait_ros="1"
fi

echo "Launching listener"
python3 "$listener" -p "$pause_file" -i "$init_path" &
listener="$!"

if [ -n "$wait_ros" ]; then
	while [ ! -f "$started_file" ]; do
		sleep 1
	done
	rm "$started_file"
fi


trap stop_listener SIGINT

i='0'

sleep 2

echo "IMPORTANT !! Press any key but '$change_options_key' followed by ENTER when you are done !!"

ask_identifier() {
	echo "Type new identifier:"
	typed_number=''
	while [ -z "$typed_number" ]; do
		read identifier
		case "$identifier" in
			*[!0-9]*)
				echo "Please, type a number:"
				;;
			"")
				typed_number="1"
				;;
			*)
				typed_number="1"
				;;
		esac
	done
}

echo ""
# Main loop
while [ -z "$end" ]; do
	if [ $paused -eq 1 ]; then
		echo "Identifier: $identifier"
		echo "To change options, press '$change_options_key' and ENTER"
		echo "------------------"
		echo "[ ENTER to START ]"
	else
		echo "ENTER to Pause"
		i=$(( $i + 1 ))
	fi

	read response

	case "$response" in
		"")
			if [ $paused -eq 1 ]; then
				paused=0
				rm "$pause_file"
			else
				printf "%b\n" "$identifier" > "$pause_file"
				paused=1
			fi
			;;
		"$change_options_key")
			# echo "Select option to change [1-3]:"
			# echo "[1] Change identifier"
			# read res_opt
			# case "$res_opt" in
				# "1")
			ask_identifier
					# ;;
			# esac
			;;
		*)
			if [ $paused -eq 0 ]; then
				printf "%b\n" "$identifier" > "$pause_file"
				paused=1
				sleep 1
			fi
			stop_listener
			end='1'
			;;
	esac
done

while ps -p "$listener" >/dev/null; do
	sleep 1
done

rm "$pause_file"

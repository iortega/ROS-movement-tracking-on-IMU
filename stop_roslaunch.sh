#!/usr/bin/env sh

MY_PATH="`dirname \"$0\"`"
xargs kill -s SIGINT < "$MY_PATH/roslaunch.pid"
rm "$MY_PATH/roslaunch.pid"

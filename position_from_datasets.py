#!/usr/bin/env python

import numpy as np
import pandas as pd
import os, os.path
import re
from position import calc
import sys
import argparse
import math


def positionFromDatasets(path=None, plotting=True, init_dataset=None,
                         init_path=None, return_pos=True, datasets=[],
                         verbose=False, filter_type='hp', Fs=66,
                         animation_only=True):

    if datasets == []:
        regex = re.compile('ds[0-9]+\.csv$')
        files = [name for name in os.listdir(path) if
                   os.path.isfile(os.path.join(path, name)) and regex.match(name)]
        for f in sorted(files):
            datasets.append(pd.read_csv(os.path.join(path,f)))

    if not isinstance(init_dataset, pd.DataFrame):
        if init_path == None:
            init_dataset = pd.read_csv(os.path.join(path, 'init.csv'))
        else:
            init_dataset = pd.read_csv(init_path,
                                       sep=',',
                                       index_col=False)

    positions=[]
    velocities=[]

    i = 0
    for ds in datasets:
        if not ds.empty:
            pos, vel = calc(dataset=ds, plotting=plotting,
                      init_data=init_dataset, filter_type=filter_type,
                      verbose=verbose, Fs=Fs, adapt_acc=True,
                      animation_only=animation_only)
            if len(pos) == 0 or (abs(pos[-1]) > 10).any() or \
                    (np.sqrt((pos**2).sum(axis=1)) < 0.05).all():
                if verbose:
                    print("Position spiked, using alternative method")
                pos2, vel2 = calc(dataset=ds, plotting=plotting,
                          init_data=init_dataset, filter_type=filter_type,
                                  verbose=verbose, Fs=Fs, adapt_acc=False,
                                  animation_only=animation_only)
                if len(pos2) > 0 and (not (abs(pos2[-1]) > 10).any() or \
                        math.sqrt((pos[-1]**2).sum()) < 0.1):
                    pos = pos2
                    vel = vel2
                else:
                    if verbose:
                        print("Alternative method not good enough")
            if (pos == np.zeros(pos.shape[1])).all():
                positions.append(np.array([]))
                velocities.append(np.array([]))
            else:
                positions.append(pos)
                velocities.append(vel)
        else:
            positions.append(np.array([]))
            velocities.append(np.array([]))
        i += 1

    return (positions, velocities)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--init-dataset", dest="init_path", help="Initialization dataset")
    parser.add_argument("-p", "--path", dest="path", help="Datasets path")
    parser.add_argument("-v", "--verbose", dest="verbose", action='store_true',
                        help="Make output more verbose")
    parser.add_argument("-a", "--all-plots", dest="all_plots", action='store_true',
                        help="Plot all graphics")

    args = parser.parse_args()

    init_path = args.init_path
    verbose = args.verbose
    animation_only = not args.all_plots
    dirname=args.path

    # init_path = "~/catkin_ws/geldi2.txt"

    if dirname == None:
        sys.exit("Datasets path cannot be void. Set one using -p or --path.")

    positionFromDatasets(path=dirname, init_path=init_path, verbose=verbose,
                         animation_only=animation_only)

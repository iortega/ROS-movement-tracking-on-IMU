#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Vector3
from std_msgs.msg import Header
from imu_data.msg import Imu_raw
from genpy.rostime import Time
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from position import calc
from func_timeout import func_timeout
from func_timeout.exceptions import FunctionTimedOut
import os.path
from datetime import datetime
import argparse

i = 0
datasets = []
y = []
x_type = []
x = []
pause = False


def callback(data):
    global pause
    if not os.path.isfile(pause_file):
        if pause:
            pause = False
        global i
        global datasets

        datasets[-1].loc[i] = [
            data.header.seq,
            data.acceleration.x,
            data.acceleration.y,
            data.acceleration.z,
            data.gyro.x,
            data.gyro.y,
            data.gyro.z,
            data.magnetometer.x,
            data.magnetometer.y,
            data.magnetometer.z,
        ]
        i += 1
    else:
        if not pause:
            pause = True
            datasets.append(pd.DataFrame(
                columns=[
                    "Counter",
                    "AccX",
                    "AccY",
                    "AccZ",
                    "GyroX",
                    "GyroY",
                    "GyroZ",
                    "MagX",
                    "MagY",
                    "MagZ",
                ]
            ))

            f = open(pause_file, 'r')

            x_type_now = f.readline()
            y_now = f.readline()
            if x_type_now:
                x_type.append(float(x_type_now))

            if y_now:
                y.append(float(y_now))

            f.close()



def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node("listener", anonymous=True)

    rospy.Subscriber("/imu", Imu_raw, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-p", "--pause-file", dest="pause_file", help="Pause \
file location")
    parser.add_argument("-v", "--verbose", help="Increase output verbosity",
                        action="store_true")

    args = parser.parse_args()

    verbose = args.verbose

    pause_file = args.pause_file

    end = False
    datasets.append(pd.DataFrame(
        columns=[
            "Counter",
            "AccX",
            "AccY",
            "AccZ",
            "GyroX",
            "GyroY",
            "GyroZ",
            "MagX",
            "MagY",
            "MagZ",
        ]
    ))

    listener()

    now = datetime.now()
    dirname = 'data-obtained/{}'.format(now.strftime('%d-%m-%Y_%H:%M:%S'))
    os.makedirs(dirname)

    if datasets[0].empty:
        datasets = datasets[1:]
    if datasets[-1].empty:
        datasets = datasets[:-1]

    if len(datasets) >= 2:
        datasets[0]['Counter'] -= datasets[0]['Counter'][datasets[0].index[0]]
        datasets[0].to_csv('{}/init.csv'.format(dirname), index=False)

        np.savetxt('{}/y.csv'.format(dirname), y, delimiter=',')
        np.savetxt('{}/x_type.csv'.format(dirname), x_type, delimiter=',')
        i = 0
        for ds in datasets[1:]:
            ds['Counter'] -= ds['Counter'][ds.index[0]]
            ds.to_csv('{}/ds{}.csv'.format(dirname, i), index=False)
            i += 1

    else:
        print("Not enough databases. Exitting")
        os.rmdir(dirname)

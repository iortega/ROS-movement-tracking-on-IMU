# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
# FEDERAL UNIVERSITY OF UBERLANDIA
# Faculty of Electrical Engineering
# Biomedical Engineering Lab
# ------------------------------------------------------------------------------
# Author: Italo Gustavo Sampaio Fernandes
# Contact: italogsfernandes@gmail.com
# Git: www.github.com/italogfernandes
# ------------------------------------------------------------------------------
# Changes author: Iñigo Ortega
# Contact: inigo@iortega.xyz
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

import re

def calc(dataset=None, plotting=True, animation_only=False,
         initialization=True, init_data='default-init.csv', return_result=True,
         verbose=True, filter_type='hp', Fs=66, adapt_acc=False):
    # filter_types = hp hp_mva lp_hamming lp_blackman

    import numpy as np
    from scipy import signal
    if plotting:
        import matplotlib.pyplot as plt
    import pandas as pd

    if animation_only and not plotting:
        animation_only = False

    # -------------------------------------------------------------------------
    # Select dataset (comment in/out)

    startTime = 2
    mag_enabled = True

    # -------------------------------------------------------------------------
    # Import data

    samplePeriod = 1/Fs

    if not isinstance(dataset, pd.DataFrame):
        regex1 = re.compile('^// Start Time: ')
        regex2 = re.compile('^// Sample rate: ')
        regex3 = re.compile('^// Scenario: ')
        regex4 = re.compile('^// Firmware Version: ')
        testing = open(dataset, 'r')
        line1 = testing.readline()
        line2 = testing.readline()
        line3 = testing.readline()
        line4 = testing.readline()
        testing.close()
        if regex1.match(line1) and regex2.match(line2) and regex3.match(line3) and regex4.match(line4):
            dataset = pd.read_csv(dataset,
                                sep='\t',
                                skiprows=4,
                                index_col=False)
        else:
            dataset = pd.read_csv(dataset)


    if isinstance(init_data, pd.DataFrame):
        initialization = True
    elif initialization:
        regex1 = re.compile('^// Start Time: ')
        regex2 = re.compile('^// Sample rate: ')
        regex3 = re.compile('^// Scenario: ')
        regex4 = re.compile('^// Firmware Version: ')
        testing = open(init_data, 'r')
        line1 = testing.readline()
        line2 = testing.readline()
        line3 = testing.readline()
        line4 = testing.readline()
        testing.close()
        if regex1.match(line1) and regex2.match(line2) and regex3.match(line3) and regex4.match(line4):
            init_data = pd.read_csv(init_data,
                                sep='\t',
                                skiprows=4,
                                index_col=False)
        else:
            init_data = pd.read_csv(init_data)

    if initialization:
        tempo_parado = int(init_data.shape[0]/Fs - startTime) #  segundos parado
        stopTime = startTime + tempo_parado + dataset.shape[0]/Fs
    else:
        tempo_parado = 3
        stopTime = dataset.shape[0]/Fs

    time = dataset.iloc[:,0].values
    gyrX = dataset.iloc[:,4].values # X
    gyrY = dataset.iloc[:,5].values # Y
    gyrZ = dataset.iloc[:,6].values # Z
    accX = dataset.iloc[:,1].values
    accY = dataset.iloc[:,2].values
    accZ = dataset.iloc[:,3].values
    magX = dataset.iloc[:,7].values
    magY = dataset.iloc[:,8].values
    magZ = dataset.iloc[:,9].values

    if initialization:
        init_mean_accX = np.mean(init_data.iloc[:,1].values[-1])
        init_mean_accY = np.mean(init_data.iloc[:,2].values[-1])
        init_mean_accZ = np.mean(init_data.iloc[:,3].values[-1])
        time = np.concatenate((init_data.iloc[:,0].values, (time + init_data.shape[0] + 1))) * samplePeriod
        gyrX = np.concatenate((init_data.iloc[:,4].values, gyrX)) # X
        gyrY = np.concatenate((init_data.iloc[:,5].values, gyrY)) # Y
        gyrZ = np.concatenate((init_data.iloc[:,6].values, gyrZ)) # Z
        if adapt_acc:
            continue_accX = accX[:1]
            continue_accY = accY[:1]
            continue_accZ = accZ[:1]
            accX = np.concatenate((init_data.iloc[:,1].values + (continue_accX - init_mean_accX), accX))
            accY = np.concatenate((init_data.iloc[:,2].values + (continue_accY - init_mean_accY), accY))
            accZ = np.concatenate((init_data.iloc[:,3].values + (continue_accZ - init_mean_accZ), accZ))
        else:
            accX = np.concatenate((init_data.iloc[:,1].values, accX))
            accY = np.concatenate((init_data.iloc[:,2].values, accY))
            accZ = np.concatenate((init_data.iloc[:,3].values, accZ))
        magX = np.concatenate((init_data.iloc[:,7].values, magX))
        magY = np.concatenate((init_data.iloc[:,8].values, magY))
        magZ = np.concatenate((init_data.iloc[:,9].values, magZ))
    else:
        time = dataset.iloc[:,0].values * samplePeriod

    # -------------------------------------------------------------------------
    # Manually frame data
    indexSel1 = time > startTime
    indexSel2 = time < stopTime
    indexSel = indexSel1 * indexSel2

    time = time[indexSel]
    gyrX = gyrX[indexSel]
    gyrY = gyrY[indexSel]
    gyrZ = gyrZ[indexSel]
    accX = accX[indexSel]
    accY = accY[indexSel]
    accZ = accZ[indexSel]
    magX = magX[indexSel]
    magY = magY[indexSel]
    magZ = magZ[indexSel]

    indexCalibStart = time > 0
    indexCalibEnd = time < tempo_parado
    indexCalib = indexCalibStart * indexCalibEnd

    # -------------------------------------------------------------------------
    # Detect stationary periods

    # Compute accelerometer magnitude
    acc_mag = np.sqrt(accX**2 + accY**2 + accZ**2)

    # HP filter accelerometer data
    filtCutOff = 0.001
    [b, a] = signal.butter(1, (2*filtCutOff)/(1/samplePeriod), 'high')
    acc_magFilt = signal.filtfilt(b, a, acc_mag)

    # Compute absolute value
    acc_magFilt = abs(acc_magFilt)

    # LP filter accelerometer data
    filtCutOff = 5
    [b, a] = signal.butter(1, (2*filtCutOff)/(1/samplePeriod), 'low')
    acc_magFilt = signal.filtfilt(b, a, acc_magFilt)

    # Descomente para ver a relação de tempo de espera para calibracao
    # plt.plot(time, acc_magFilt)
    # plt.plot(time[:(tempo_parado)*Fs], acc_magFilt[:(tempo_parado)*Fs])

    if filter_type == "hp":
        # Threshold detection
        stationaty_start_time = acc_magFilt[:(tempo_parado)*Fs]
        statistical_stationary_threshold = np.mean(stationaty_start_time) + 2*np.std(stationaty_start_time)
        stationary_threshold = 0.048

        if verbose:
            print('Limiar Calculado = %.4f + 2 * %.4f = %.4f' % (np.mean(stationaty_start_time),
                                                                np.std(stationaty_start_time),
                                                                statistical_stationary_threshold))
            print('Limiar fixo = %.4f' % (stationary_threshold*2))

        stationary = acc_magFilt < statistical_stationary_threshold
        # stationary = acc_magFilt < stationary_threshold

    elif filter_type == 'hp_mva':
        # LP filter accelerometer data using MVA
        filtCutOff = 5
        filtCutOff_nyq_norm = filtCutOff/(Fs/2.0)
        N = np.sqrt(0.196202+filtCutOff_nyq_norm**2)/filtCutOff_nyq_norm
        N = np.round(N).astype(int)
        N = 8
        mva_window = np.ones((N,1))
        mva_window = mva_window/np.sum(mva_window)

        signal_window = np.zeros((N,1))
        acc_magFilt_mva = np.zeros(acc_magFilt.shape)
        for n in  range(len(acc_magFilt)):
            signal_window[0:-1] = signal_window[1:]
            signal_window[-1] = acc_magFilt[n]
            acc_magFilt_mva[n] = np.sum(signal_window * mva_window)

        # Threshold detection
        calib_data = acc_magFilt_mva[indexCalib]
        statistical_stationary_threshold = np.mean(calib_data) + 2*np.std(calib_data)

        if verbose:
            print('Limiar Calculado = %.4f + 2 * %.4f = %.4f' % (np.mean(calib_data),
                                                                 np.std(calib_data),
                                                                 statistical_stationary_threshold))

        stationary = acc_magFilt_mva < statistical_stationary_threshold


    elif filter_type == 'lp_hamming':
        # LP filter accelerometer data using Hamming Window
        N = 8
        ham_window = signal.windows.hamming(N)
        ham_window = ham_window/np.sum(ham_window)

        signal_window = np.zeros((N,1))
        a = [1, 2, 3, 4]

        acc_magFilt_ham = np.zeros(acc_magFilt.shape)
        for n in  range(len(acc_magFilt)):
            signal_window[0:-1] = signal_window[1:]
            signal_window[-1] = acc_magFilt[n]
            acc_magFilt_ham[n] = np.sum(signal_window * ham_window)/N

        # Threshold detection
        calib_data = acc_magFilt_ham[indexCalib]
        statistical_stationary_threshold = np.mean(calib_data) + 2*np.std(calib_data)

        if verbose:
            print('Limiar Calculado = %.4f + 2 * %.4f = %.4f' % (np.mean(calib_data),
                                                                 np.std(calib_data),
                                                                 statistical_stationary_threshold))

        stationary = acc_magFilt_ham < statistical_stationary_threshold

    elif filter_type == 'lp_blackman':
        # LP filter accelerometer data using blackman Window
        N = 8
        bm_window = signal.windows.blackman(N)
        bm_window = bm_window/np.sum(bm_window)

        signal_window = np.zeros((N,1))
        a = [1, 2, 3, 4]

        acc_magFilt_bm = np.zeros(acc_magFilt.shape)
        for n in  range(len(acc_magFilt)):
            signal_window[0:-1] = signal_window[1:]
            signal_window[-1] = acc_magFilt[n]
            acc_magFilt_bm[n] = np.sum(signal_window * bm_window)/N

        # Threshold detection
        calib_data = acc_magFilt_bm[indexCalib]
        statistical_stationary_threshold = np.mean(calib_data) + 2*np.std(calib_data)

        if verbose:
            print('Limiar Calculado = %.4f + 2 * %.4f = %.4f' % (np.mean(calib_data),
                                                                 np.std(calib_data),
                                                                 statistical_stationary_threshold))

        stationary = acc_magFilt_bm < statistical_stationary_threshold
        print(stationary)



    # -------------------------------------------------------------------------
    # Plot data raw sensor data and stationary periods
    if plotting and not animation_only:
        plt.figure(figsize=(20,10))
        plt.suptitle('Sensor Data', fontsize=14)
        ax1 = plt.subplot(2+mag_enabled,1,1)
        plt.grid()
        plt.plot(time, gyrX, 'r')
        plt.plot(time, gyrY, 'g')
        plt.plot(time, gyrZ, 'b')
        plt.title('Gyroscope')
        plt.ylabel('Angular velocity (º/s)')
        plt.legend(labels=['X', 'Y', 'Z'])


        plt.subplot(2+mag_enabled,1,2,sharex=ax1)
        plt.grid()
        plt.plot(time, accX, 'r')
        plt.plot(time, accY, 'g')
        plt.plot(time, accZ, 'b')
        plt.plot(time, acc_magFilt, ':k')
        plt.plot(time, stationary.astype(np.uint8)*acc_magFilt.max(), 'k', linewidth= 2)
        plt.title('Accelerometer')
        plt.ylabel('Acceleration (g)')
        plt.legend(['X', 'Y', 'Z', 'Filtered', 'Stationary'])

        if mag_enabled:
            plt.subplot(3,1,3,sharex=ax1)
            plt.grid()
            plt.plot(time, magX, 'r')
            plt.plot(time, magY, 'g')
            plt.plot(time, magZ, 'b')
            plt.title('Magnetrometer')
            plt.ylabel('Magnetic Flux Density  (G)')
            plt.legend(['X', 'Y', 'Z'])

        plt.xlabel('Time (s)')


    # -------------------------------------------------------------------------
    # Compute orientation
    from madgwickahrs import MadgwickAHRS

    quat = [None] * len(time)
    AHRSalgorithm = MadgwickAHRS(sampleperiod=1/Fs)

    # Initial convergence
    initPeriod = tempo_parado # usually 2 seconds
    indexSel = time < (tempo_parado+time[0])
    for i in range(2000):
        AHRSalgorithm.update_imu([0, 0, 0],
                            [accX[indexSel].mean(), accY[indexSel].mean(), accZ[indexSel].mean()])

    # For all data
    for t in range(len(time)):
        if stationary[t]:
            AHRSalgorithm.beta = 0.5
        else:
            AHRSalgorithm.beta = 0

        AHRSalgorithm.update_imu(
                np.deg2rad([gyrX[t], gyrY[t], gyrZ[t]]),
                [accX[t], accY[t], accZ[t]])
        quat[t] = AHRSalgorithm.quaternion

    quats = []
    for quat_obj in quat:
        quats.append(quat_obj.q)
    quats =np.array(quats)
    quat = quats
    # -------------------------------------------------------------------------
    # Compute translational accelerations
    import quaternion_toolbox
    # Rotate body accelerations to Earth frame
    a = np.array([accX, accY, accZ]).T
    acc = quaternion_toolbox.rotate(a, quaternion_toolbox.conjugate(quat))

    # # Remove gravity from measurements
    # acc = acc - [zeros(length(time), 2) ones(length(time), 1)]     # unnecessary due to velocity integral drift compensation

    # Convert acceleration measurements to m/s/s
    acc = acc * 9.81

    # Plot translational accelerations
    if plotting and not animation_only:
        plt.figure(figsize=(20,10))
        plt.suptitle('Accelerations', fontsize=14)
        plt.grid()

        plt.plot(time, acc[:,0], 'r')
        plt.plot(time, acc[:,1], 'g')
        plt.plot(time, acc[:,2], 'b')
        plt.title('Acceleration')
        plt.xlabel('Time (s)')
        plt.ylabel('Acceleration (m/s/s)')
        plt.legend(('X', 'Y', 'Z'))



    # -------------------------------------------------------------------------
    # Compute translational velocities

    acc[:,2] = acc[:,2] - 9.81

    # Integrate acceleration to yield velocity
    vel = np.zeros(np.shape(acc))
    for t in range(1,len(vel)):
        vel[t,:] = vel[t-1,:] + acc[t,:] * samplePeriod
        if stationary[t]:
            vel[t,:] = np.zeros((3))    # force zero velocity when foot stationary


    # Compute integral drift during non-stationary periods

    velDrift = np.zeros(np.shape(vel))

    d = np.append(arr = [0], values = np.diff(stationary.astype(np.int8)))
    stationaryStart = np.where( d == -1)
    stationaryEnd =  np.where( d == 1)
    stationaryStart = np.array(stationaryStart)[0]
    stationaryEnd = np.array(stationaryEnd)[0]

    if len(stationaryStart) < len(stationaryEnd):
        stationaryStart = np.concatenate(([max(0, stationaryEnd[0]-10)], stationaryStart))
    elif len(stationaryStart) > 0 and len(stationaryEnd) > 0 and stationaryStart[0] > stationaryEnd[0]:
        stationaryStart, stationaryEnd = stationaryEnd, stationaryStart
    elif len(stationaryStart) > len(stationaryEnd):
        stationaryStart = stationaryStart[:-1]

    for i in range(len(stationaryEnd)):
        driftRate = vel[stationaryEnd[i]-1, :] / (stationaryEnd[i] - stationaryStart[i])
        enum = np.arange(0, stationaryEnd[i] - stationaryStart[i])
        enum_t = enum.reshape((1,len(enum)))
        driftRate_t = driftRate.reshape((1,len(driftRate)))
        drift = enum_t.T * driftRate_t
        velDrift[stationaryStart[i]:stationaryEnd[i], :] = drift

    # Remove integral drift
    vel = vel - velDrift
    vel -= vel[int(tempo_parado*Fs)]
    for i in range(tempo_parado*Fs):
        vel[i,:] = np.zeros((3))

    # Remove more drift
    threshold = 0.0
    lookBack = 5
    for i in range(lookBack,len(vel)):
        if (vel[i] > threshold).any():
            wrongStationary = np.equal(vel[i-lookBack:i], vel[i]).T
            wrong = [v for v in range(len(wrongStationary)) if wrongStationary[v].all()]
            if len(wrong) > 0:
                vel[i-lookBack:, wrong] -= vel[i, wrong]

    # Plot translational velocity
    if plotting and not animation_only:
        plt.figure(figsize=(20,10))
        plt.suptitle('Velocity', fontsize=14)
        plt.grid()
        plt.plot(time, vel[:,0], 'r')
        plt.plot(time, vel[:,1], 'g')
        plt.plot(time, vel[:,2], 'b')
        plt.title('Velocity')
        plt.xlabel('Time (s)')
        plt.ylabel('Velocity (m/s)')
        plt.legend(('X', 'Y', 'Z'))


    # -------------------------------------------------------------------------
    # Compute translational position

    # Integrate velocity to yield position
    pos = np.zeros(np.shape(vel))
    for t in range(1,len(pos)):
        pos[t,:] = pos[t-1,:] + vel[t,:] * samplePeriod    # integrate velocity to yield position


    # Plot translational position
    if plotting and not animation_only:
        plt.figure(figsize=(20,10))
        plt.suptitle('Position', fontsize=14)
        plt.grid()
        plt.plot(time, pos[:,0], 'r')
        plt.plot(time, pos[:,1], 'g')
        plt.plot(time, pos[:,2], 'b')
        plt.title('Position')
        plt.xlabel('Time (s)')
        plt.ylabel('Position (m)')
        plt.legend(('X', 'Y', 'Z'))

    if verbose:
        print('Erro em Z: %.4f' % abs(pos[-1, 2]))
    # -------------------------------------------------------------------------
    #  Plot 3D foot trajectory

    # # Remove stationary periods from data to plot
    # posPlot = pos(find(~stationary), :)
    # quatPlot = quat(find(~stationary), :)
    posPlot = pos
    quatPlot = quat

    # Extend final sample to delay end of animation
    extraTime = 20
    onesVector = np.ones((extraTime*Fs, 1))
    #TODO: usar pading
    # np.pad()
    #posPlot = np.append(arr = posPlot, values = onesVector * posPlot[-1, :])
    #quatPlot = np.append(arr = quatPlot, values = onesVector * quatPlot[-1, :])

    # -------------------------------------------------------------------------
    # Create 6 DOF animation
    # TODO: improve it

    import numpy as np
    if plotting:
        import matplotlib.pyplot as plt
    import mpl_toolkits.mplot3d.axes3d as p3
    import matplotlib.animation as animation


    posPlot = posPlot.T

    if initialization:
        indexes = stationaryStart > init_data.shape[0]

        if verbose:
            print(init_data.shape)
            print(len(pos))
            print(pos)

        big_non_stationary_areas = (stationaryEnd - stationaryStart) > 20

        if not True in big_non_stationary_areas:
            posRelevant = pos
            velRelevant = vel
        else:
            posRelevant = pos[stationaryStart[big_non_stationary_areas][0]:
                              stationaryEnd[big_non_stationary_areas][-1]]
            velRelevant = vel[stationaryStart[big_non_stationary_areas][0]:
                              stationaryEnd[big_non_stationary_areas][-1]]



    #
    # Attaching 3D axis to the figure
    if plotting:
        fig = plt.figure()
        ax = p3.Axes3D(fig)

    if len(stationaryStart) == 0:
        start = 0
    else:
        start = stationaryStart[0]
    data_x = posPlot[0,start:]
    data_y = posPlot[1,start:]
    data_z = posPlot[2,start:]
    size = max(max(abs(data_x)), max(abs(data_y)), max(abs(data_z)))
    if abs(size) == 0:
        size = 1
    # Creating fifty line objects.
    # NOTE: Can't pass empty arrays into 3d version of plot()
    if plotting:
        line = ax.plot(data_x, data_y, data_z)
        line = line[0]

        # Setting the axes properties
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')

        ax.set_title('3D Animation')

        ax.set_xlim3d([-size, size])
        ax.set_ylim3d([-size, size])
        ax.set_zlim3d([-size, size])



    def run_animation():
        anim_running = True

        def onClick(event):
            if event.key == ' ':
                nonlocal anim_running
                if anim_running:
                    line_ani.event_source.stop()
                    anim_running = False
                else:
                    line_ani.event_source.start()
                    anim_running = True

        def update_lines(num):
            # NOTE: there is no .set_data() for 3 dim data...
            index = num*10
            line.set_data(posPlot[0:2, :index])
            line.set_3d_properties(posPlot[2,:index])
            return line

        fig.canvas.mpl_connect('key_press_event', onClick)

        # Creating the Animation object
        line_ani = animation.FuncAnimation(fig=fig, func=update_lines,
                                        frames = int(max(posPlot.shape)/10),
                                        fargs=None,
                                        interval=50, blit=False)


    if plotting:
        run_animation()

        plt.show()

    if return_result:
        return (posRelevant, velRelevant)


if __name__ == "__main__":
    calc()

# ROS-movement-tracking-on-IMU

## Euskaraz
Biltegi honetan ikasketa automatikoan behar diren datubaseak egiteko erabiltzen
diren iturburu-kodeak daude. Ordea, hemengo gidoi guztiak ez dira datubaseen
sorpena bakarrik egiteko, baina horretarako erabiltzen dira gehienbat.

Fitxategien azalpena:
- **generate_data.py** (KLI euskarria): Datubaseak sortzeko Python gidoi nagusia. Horretarako,
	IMUen bidez sortutako beste datubase txikiago batzuk erabiltzen ditu.
	Datubase hauek dataset_creator.sh (edo listener.py) bidez sortzen dira.
- **dataset_creator.sh**: Datubaseak sortzeko frontend shell gidoi nagusia.
- **listener.py** (KLI euskarria): IMUaren driverrak emandako datuak ROS bidez jasotzen dituen
	Python gidoia, hauek data-obtained/ _gaurkodata_ fitxategian gordez.
- **position_from_datasets.py** (KLI euskarria): Python gidoi honek posizioa (eta abiadura)
	kalkulatzeko erabiltzen den funtzio bat du, honela, datubaseen karpeta bat
	edo Pandas DataFrame-n zerrenda bat emanda, hauek guztien posizioa eta
	abiadura kalkula dezake, Python bidez itzuliz edota grafikoen bidez
	errepresentatuz.
- **position.py** (KLI euskarria): Posizioa eta abiadura azelerazio, giroskopio eta
	magnetometrotik kalkulatzen dituen Python gidoia, hauek Python bidez itzuliz
	edota grafikoen bidez errepresentatuz. Gidoi hau University of Uberlandiako
	Italo Gustavo Sampaio Fernandes-en lanaren adarkatze bat da eta honetan dago
	oinarrituta.

	- **madgwickahrs.py**: **position.py** -ren dependentzia. Orientazioa
	kalkulatzeko erabilita.
	- **quaternion.py**, **quaternion_toolbox.py**: **madgwickahrs.py** -ren depentziak.

- **stop_roslaunch.sh**: **dataset_creator.sh** gidoiaren ondoren ROS prozesua eteteko
	erabiltzen den gidoia.

### Adibideak

Datuak jasotzeko:
```
$ ./dataset_creator.sh; ./stop_roslaunch.sh
```

Sare neuronaletarako datubaseak sortzeko:
```
$ python3 generate_data.py -p data-obtained/train/ -y y_train.csv -x x_train.csv -i data-obtained/train/17-08-2020_16:50:08/init.csv
```
Ez badira -y edo -x aukerak eman, x.csv eta y.csv izenak erabiliko dira. -i
aukera ematen ez bada, datubaseen karpeta bakoitzerako dagokion init.csv fitxategia
erabiliko dira.


13-08-2020_19:21:31 momentuko datubaseko posizioak bistaratu:
```
$ python3 position_from_datasets.py -p data-obtained/13-08-2020_19:21:31/
```

## English

This repository holds the necessary source files in order to create a dataset
for later to be used on a machine learning task. However, the scripts available
here are not only for dataset creation, but they are currently used for that.

File description:
- **generate_data.py** (CLI support): The main python script to use in order to generate the
	big dataset from smaller datasets directly obtained from IMU that later
	would be used to feed machine learning architecture.
- **dataset_creator.sh**: The shell script used as a frontend to create datasets.
- **listener.py** (CLI support): Python script used to read data provided by ROS through a topic
	created by the IMU driver.
- **position_from_datasets.py** (CLI support): This Python script holds a function used
	calculate the position (and velocity) from given datasets (folder with files
	created by listener.py or as a list of Pandas DataFrames).
- **position.py** (CLI support): The main Python script that creates position and velocity out
	of acceleration, gyroscopy and magnetometer data. This is a fork of the
	source made by Italo Gustavo Sampaio Fernandes from University of
	Uberlandia.

	- **madgwickahrs.py**: A dependency of **position.py**. Used to compute
	orientation.
	- **quaternion.py**, **quaternion_toolbox.py**: Dependency of **madgwickahrs.py**.

- **stop_roslaunch.sh**: Script to be run in order to terminate the ROS process
	after **dataset_creator.sh** is exitted.

### Examples

To receive data:
```
$ ./dataset_creator.sh; ./stop_roslaunch.sh
```

To create datasets for Neural Networks:
```
$ python3 generate_data.py -p data-obtained/train/ -y y_train.csv -x x_train.csv -i data-obtained/train/17-08-2020_16:50:08/init.csv
```
If no -y or -x options are used x.csv and y.csv names are used and if no -i is
used, the init.csv file corresponding to each dataset folder is used.


Plot positions from 13-08-2020_19:21:31 dataset:
```
$ python3 position_from_datasets.py -p data-obtained/13-08-2020_19:21:31/
```

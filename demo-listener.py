#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Vector3
from std_msgs.msg import Header
from imu_data.msg import Imu_raw
from genpy.rostime import Time
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from func_timeout import func_timeout
from func_timeout.exceptions import FunctionTimedOut
import os.path
from datetime import datetime
import argparse
from position_from_datasets import positionFromDatasets

from tensorflow.keras.models import load_model
from tensorflow.keras.layers import LeakyReLU as LR

from generate_data import generate


i = 0
dataset = None
pause = False
Fs = 66
models = []
# models.append(load_model('models/model0.h5', custom_objects={'LeakyReLU': LR}))
# models.append(load_model('models/model1.h5', custom_objects={'LeakyReLU': LR}))
models.append(load_model('models/model0-gan.h5', custom_objects={'LeakyReLU': LR}))
models.append(load_model('models/model1-gan.h5', custom_objects={'LeakyReLU': LR}))

def callback(data):
    global pause
    if not os.path.isfile(pause_file):
        if pause:
            pause = False
        else:
            global i
            global dataset

            dataset.loc[i] = [
                data.header.seq,
                data.acceleration.x,
                data.acceleration.y,
                data.acceleration.z,
                data.gyro.x,
                data.gyro.y,
                data.gyro.z,
                data.magnetometer.x,
                data.magnetometer.y,
                data.magnetometer.z,
            ]
            i += 1
    else:
        if not pause:
            pause = True

            f = open(pause_file, 'r')

            x_type_now = f.readline()

            f.close()
            if x_type_now:
                global Fs
                init_dataset = pd.read_csv(init_path)
                dataset['Counter'] -= dataset['Counter'][dataset.index[0]]

                x, _ = generate(datasets=[dataset], init_dataset=init_dataset,
                             Fs=Fs, return_data=True, stats=False)


                global models
                prediction = models[int(x_type_now)].predict(np.array(x))
                if prediction > 0.5:
                    print("Movement marked as correct")
                else:
                    print("Movement marked as incorrect")
                print("Output: {:1.2f}".format(prediction[0][0]))

            dataset = pd.DataFrame(
                columns=[
                    "Counter",
                    "AccX",
                    "AccY",
                    "AccZ",
                    "GyroX",
                    "GyroY",
                    "GyroZ",
                    "MagX",
                    "MagY",
                    "MagZ",
                ]
            )



def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node("listener", anonymous=True)

    rospy.Subscriber("/imu", Imu_raw, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-p", "--pause-file", dest="pause_file", help="Pause \
file location")
    parser.add_argument("-v", "--verbose", help="Increase output verbosity",
                        action="store_true")
    parser.add_argument("-i", "--init-dataset", dest="init_path", help="Initialization dataset")

    args = parser.parse_args()

    verbose = args.verbose
    pause_file = args.pause_file
    init_path = args.init_path

    end = False
    datasets = pd.DataFrame(
        columns=[
            "Counter",
            "AccX",
            "AccY",
            "AccZ",
            "GyroX",
            "GyroY",
            "GyroZ",
            "MagX",
            "MagY",
            "MagZ",
        ]
    )

    listener()

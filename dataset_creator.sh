#!/usr/bin/env bash

MY_PATH="`dirname \"$0\"`"
CATKIN_FOLDER="$HOME/catkin_ws"
fifo="/tmp/ros-dataset-creator$(date +%s%N)"
started_file="$MY_PATH/ros-started-transmitting$(date +%s%N)"
identifier="0"
correct="1"
pause_file="$MY_PATH/pause$(date +%s%N)"
rospid_file="$MY_PATH/roslaunch.pid"
listener="$MY_PATH/listener.py"
change_options_key='z'
delete_datasets_key='-'
toremove=''

# Function to call to stop listener in case of SIGINT
stop_listener() {
	kill -s INT "$listener"
	end='1'
}

paused='1'
if [ $paused -eq 1 ]; then
	touch "$pause_file"
else
	rm "$pause_file" 2>/dev/null
fi

# Running the tools needed
echo "Sourcing setup.bash..."
. "$CATKIN_FOLDER/devel/setup.bash"
if [ ! -f "$MY_PATH/roslaunch.pid" ]; then
	echo "Launching ROS y3space_driver..."
	echo "====================================================
Keep the IMU looking North, still and DON'T TOUCH IT
===================================================="
	sleep 1
	echo "Starting ROS in 3"
	sleep 1
	echo "Starting ROS in 2"
	sleep 1
	echo "Starting ROS in 1"
	sleep 1
	{
		master="$$"
		mkfifo "$fifo"
		roslaunch y3space_driver driver.launch > "$fifo" &
		roslaunch="$!"
		echo "Saving ROS pid on roslaunch.pid"
		echo "$roslaunch" > "$rospid_file"
		while read -r line; do
			if [ "$line" = "START" ]; then
				touch "$started_file"
			fi
		done < "$fifo"
		rm "$fifo"
	} &
	wait_ros="1"
fi

echo "Launching listener"
python3 "$listener" -p "$pause_file" &
listener="$!"

if [ -n "$wait_ros" ]; then
	while [ ! -f "$started_file" ]; do
		sleep 1
	done
	rm "$started_file"
fi


trap stop_listener SIGINT

i='-1'

# Initialization capture
echo "=================================================================
First, initialization data must be captured, DON'T MOVE YOUR IMU!
================================================================="
echo ""
printf -- "$i "
echo "ENTER to START CAPTURING"
i=$(( $i + 1 ))

read response

if [ -z "$response" ]; then
	paused=0
	rm "$pause_file"
	sleep 5
	touch "$pause_file"
	paused=1
	printf "%b\n\n" "INITIALIZATION DONE"
else
	stop_listener
	end='1'
fi

echo "IMPORTANT !! Press any key but '$change_options_key' or '$delete_datasets_key' followed by ENTER when you are done !!"

ask_identifier() {
	echo "Type new identifier:"
	typed_number=''
	while [ -z "$typed_number" ]; do
		read identifier
		case "$identifier" in
			*[!0-9]*)
				echo "Please, type a number:"
				;;
			"")
				typed_number="1"
				;;
			*)
				typed_number="1"
				;;
		esac
	done
}

ask_correct() {
	echo "Type new correct value (0: incorrect; 1: correct):"
	typed_number=''
	while [ -z "$typed_number" ]; do
		read correct
		case "$correct" in
			*[!0-9]*)
				echo "Please, type a number:"
				;;
			"")
				typed_number="1"
				;;
			*)
				typed_number="1"
				;;
		esac
	done
}

echo ""
# Main loop
while [ -z "$end" ]; do
	echo "DATASET: $i"
	if [ $paused -eq 1 ]; then
		echo "Identifier: $identifier"
		echo "Correct: $correct"
		echo "To change options, press '$change_options_key' and ENTER"
		echo "To delete datasets, press '$delete_datasets_key' and ENTER"
		echo "------------------"
		echo "[ ENTER to START ]"
	else
		echo "ENTER to Pause"
		i=$(( $i + 1 ))
	fi

	read response

	case "$response" in
		"")
			if [ $paused -eq 1 ]; then
				paused=0
				rm "$pause_file"
			else
				printf "%b\n%b\n" "$identifier" "$correct" > "$pause_file"
				paused=1
			fi
			;;
		"$delete_datasets_key")
			printf "Type dataset numbers to remove (default: last one): "
			isnumber=''
			selected=''
			while [ -z "$isnumber" ]; do
				read selected
				if [ -z "$selected" ]; then
					selected="$(( $i - 1 ))"
				fi
				case "$selected" in
					*[!0-9]*)
						# Not number
						printf "Please, type a positive number (default: last one): "
						;;
					*)
						# Number
						if [ "$selected" -le "$i" ]; then
							isnumber="1"
						else
							echo "Cannot remove a non existant dataset"
						fi
						;;
				esac
			done
			toremove="$toremove\n$selected"
			echo "Datasets to be deleted: $(printf "$toremove" | tr '\n' ' ')"
			echo ""
			;;
		"$change_options_key")
			echo "Select option to change [1-3]:"
			echo "[1] Change identifier"
			echo "[2] Change correctness"
			echo "[3] Change both"
			read res_opt
			case "$res_opt" in
				"1")
					ask_identifier
					;;
				"2")
					ask_correct
					;;
				"3")
					ask_identifier
					ask_correct
					;;
			esac
			;;
		*)
			if [ $paused -eq 0 ]; then
				printf "%b\n%b\n" "$identifier" "$correct" > "$pause_file"
				paused=1
				sleep 1
			fi
			stop_listener
			end='1'
			;;
	esac
done

while ps -p "$listener" >/dev/null; do
	sleep 1
done
last_results=$(ls -t "$MY_PATH/data-obtained/" | head -n 1)
printf "$toremove" | xargs -I{} rm "$MY_PATH/data-obtained/$last_results/ds{}.csv"
printf "$toremove" | xargs -I{} echo '1 + {}' | bc | sort -r | xargs -I{} sed -i '{}d' "$MY_PATH/data-obtained/$last_results/y.csv"
printf "$toremove" | xargs -I{} echo '1 + {}' | bc | sort -r | xargs -I{} sed -i '{}d' "$MY_PATH/data-obtained/$last_results/x_type.csv"

rm "$pause_file"

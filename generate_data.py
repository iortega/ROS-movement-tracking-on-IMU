#!/usr/bin/env python

from position_from_datasets import positionFromDatasets
import numpy as np
import pandas as pd
import re
import os, os.path
import argparse
import warnings
import sys
import matplotlib.pyplot as plt
from multiprocessing_on_dill import Pool
import tqdm

def getPosVelFromDatasets(datasets, init_dataset):
    positions = []
    velocities = []

    if not isinstance(init_dataset, pd.DataFrame):
        init_dataset = pd.read_csv(init_dataset)

    zero_positions = []
    for dataset in datasets:
        pos, vel = positionFromDatasets(init_dataset=init_dataset,
                                        plotting=False,
                                        datasets=[dataset])
        pos = pos[0].clip(-13, 13)
        vel = vel[0].clip(-13, 13)

        if len(pos) != 0:
            positions.append(pos)
            velocities.append(vel)


    return (positions, velocities)

def getPosVelLabel(path, recalc_pos=False, init_dataset=None):
    regex_ds = re.compile('ds[0-9]+\.csv$')
    regex_posvel = re.compile('(pos|vel)[0-9]+\.csv$')
    positions = []
    velocities = []
    files_posvel = [name for name in os.listdir(path) if
                    os.path.isfile(os.path.join(path, name)) and regex_posvel.match(name)]
    files = [name for name in os.listdir(path) if
               os.path.isfile(os.path.join(path, name)) and regex_ds.match(name)]

    if not isinstance(init_dataset, pd.DataFrame):
        init_dataset = pd.read_csv(os.path.join(path, 'init.csv'))

    zero_positions = []
    i = 0
    for f in sorted(files, key=lambda x: int(x[2:x.index(".")])):
        posname = 'pos' + f[2:]
        velname = 'vel' + f[2:]
        if not recalc_pos and posname in files_posvel and \
                velname in files_posvel:
            with warnings.catch_warnings():
                warnings.filterwarnings('ignore', category=UserWarning)
                pos = np.loadtxt(os.path.join(path, posname), delimiter=',')
                vel = np.loadtxt(os.path.join(path, velname), delimiter=',')
        else:
            pos, vel = positionFromDatasets(init_dataset=init_dataset,
                                            plotting=False,
                                            datasets=[pd.read_csv(os.path.join(path,f))])
            pos = pos[0].clip(-13, 13)
            vel = vel[0].clip(-13, 13)
            np.savetxt(os.path.join(path, posname), pos, delimiter=',')
            np.savetxt(os.path.join(path, velname), vel, delimiter=',')

        if len(pos) != 0:
            positions.append(pos)
            velocities.append(vel)
        else:
            zero_positions.append(i)

        i += 1

    new_y = np.delete(np.loadtxt(os.path.join(path, 'y.csv')),
                      zero_positions)

    return (positions, velocities, new_y)

def generate(data_path='data-obtained', datasets=[], recalc_pos=False, init_dataset=None,
             x_name='x.csv', y_name='y.csv', stats=True, Fs=66, return_data = False):

    regex_ds = re.compile('ds[0-9]+\.csv$')
    positions = []
    velocities = []
    y = np.array([])

    if not isinstance(init_dataset, pd.DataFrame):
        if init_dataset:
            init_dataset = pd.read_csv(init_dataset)

    if len(datasets) == 0:
        dirs = os.listdir(data_path)

        # Posizio, Abiadura eta Klaseak paraleloki lortu
        f = lambda x: getPosVelLabel(os.path.join(data_path, x),
                                    recalc_pos=recalc_pos,
                                    init_dataset=init_dataset)

        res = []
        pool = Pool(os.cpu_count())
        for x in tqdm.tqdm(pool.imap(f, sorted(dirs)), total=len(dirs)):
            res.append(x)

        pool.close()
        pool.join()

        # Emaitzak gorde
        for i in res:
            positions.extend(i[0])
            velocities.extend(i[1])
            y = np.concatenate((y, i[2]))

        # Datubaseak kargatu
        dirs = os.listdir(data_path)
        for dirname in dirs:
            path = os.path.join(data_path, dirname)
            files = [name for name in os.listdir(path) if
                    os.path.isfile(os.path.join(path, name)) and regex_ds.match(name)]

            for f in sorted(files, key=lambda x: int(x[2:x.index(".")])):
                datasets.append(pd.read_csv(os.path.join(path,f)))
    else:
        positions, velocities = getPosVelFromDatasets(datasets, init_dataset)



    if stats:
        # Estatistikak bistaratzeko prestaketa
        whole = np.hstack([np.hstack(pos) for pos in positions])

        nonzero = np.count_nonzero(y)
        print("Correct movements:", nonzero)
        print("Incorrect movements:", y.shape[0] - nonzero)

        # Estatistikak
        results = []
        objects = []
        for r in range(-13, 13):
            sel = (whole > r) * (whole < r+1)
            results.append(len(whole[sel]))
            objects.append('{}'.format(r))

        y_pos = np.arange(len(objects))
        plt.barh(y_pos, results, align='center')
        plt.ylabel('Position ranges', fontsize=14)
        plt.xlabel('Number of occurrences', fontsize=14)
        plt.title('Number of occurrences of each position value range', fontsize=15)
        plt.yticks(y_pos, objects, fontsize=14)
        plt.tick_params(axis='x', labelsize=14)
        plt.show()

        results = []
        objects = []
        for r in range(-13, 13):
            sel = (whole > r) * (whole < r+1)
            results.append(len(whole[sel]))
            objects.append('{}'.format(r))

        y_pos = np.arange(len(objects))
        plt.barh(y_pos, results, align='center')
        plt.grid()
        plt.xscale('log')
        plt.ylabel('Position ranges', fontsize=14)
        plt.xlabel('Number of occurrences (log)', fontsize=14)
        plt.title('Number of occurrences of each position value range', fontsize=15)
        plt.yticks(y_pos, objects, fontsize=14)
        plt.tick_params(axis='x', labelsize=14)
        plt.show()

        results = []
        objects = []
        for r in np.linspace(-1.5, 0.5, 20):
            sel = (whole > r) * (whole < r+1)
            results.append(len(whole[sel]))
            objects.append('{:2.2f}'.format(r))

        y_pos = np.arange(len(objects))
        plt.barh(y_pos, results, align='center')
        plt.ylabel('Position ranges', fontsize=14)
        plt.xlabel('Number of occurrences', fontsize=14)
        plt.title('Number of occurrences of each position value range', fontsize=15)
        plt.yticks(y_pos, objects, fontsize=14)
        plt.tick_params(axis='x', labelsize=14)
        plt.show()

    x = []

    # Formatu egokira bihurtu  datuak
    warnings.filterwarnings('ignore', 'Polyfit may be poorly conditioned')
    for i in range(len(positions)):
        pos = positions[i]
        if len(pos) > 0:
            vel = velocities[i]
            dataset = datasets[i]
            acc = np.array((dataset.AccX, dataset.AccY,
                           dataset.AccZ)).T
            mag = np.array((dataset.MagX, dataset.MagY,
                             dataset.MagZ)).T
            gyro = np.array((dataset.GyroX, dataset.GyroY,
                             dataset.GyroZ)).T

            num_zeros_pos = max(0, Fs*10*3 - pos.shape[0]*pos.shape[1])
            num_zeros_vel = max(0, Fs*10*3 - vel.shape[0]*vel.shape[1])
            num_zeros_acc = max(0, Fs*10*3 - acc.shape[0]*acc.shape[1])
            num_zeros_gyro = max(0, Fs*10*3 - gyro.shape[0]*gyro.shape[1])
            num_zeros_mag = max(0, Fs*10*3 - mag.shape[0]*mag.shape[1])
            flat = np.hstack((np.hstack(pos[:Fs*10]), np.zeros(num_zeros_pos),
                              # np.hstack(vel[:Fs*10]), np.zeros(num_zeros_vel),
                              np.hstack(acc[:Fs*10]), np.zeros(num_zeros_acc),
                              np.hstack(gyro[:Fs*10]), np.zeros(num_zeros_gyro),
                              # np.hstack(mag[:Fs*10], np.zeros(num_zeros_mag)),
                              ))

            x.append(flat)
        else:
            print("Incorrect position, skipping")

    if return_data:
        return (x, y)
    else:
        np.savetxt(x_name, x, delimiter=',')
        np.savetxt(y_name, y, delimiter=',')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-p", "--data-path", dest="data_path", help="Datasets path")
    parser.add_argument("-r", "--recalculate-position", dest="recalc_pos",
                        help="Recalculate position", action="store_true")
    parser.add_argument("-i", "--init-dataset", dest="init_path", help="Initialization dataset")
    parser.add_argument("-x", "--x-name", dest="x_name", help="x dataset name")
    parser.add_argument("-y", "--y-name", dest="y_name", help="y dataset name")

    args = parser.parse_args()

    data_path = args.data_path
    recalc_pos = args.recalc_pos
    init_path = args.init_path
    x_name = args.x_name
    y_name = args.y_name

    generate(data_path=data_path, recalc_pos=recalc_pos, init_dataset=init_path,
             x_name=x_name, y_name=y_name)
